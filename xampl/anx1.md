# Annexe 1 — exercice 1 {#anx1exo1}

(extrait de la documentation Python, chapitre « Structures de données, Compléments sur les listes » [`https://docs.python.org/fr/3/tutorial/datastructures`](https://docs.python.org/fr/3/tutorial/datastructures.html))

Utilisation des listes comme des piles
--------------------------------------

Les méthodes des listes rendent très facile leur utilisation comme des piles, où le dernier élément ajouté est le premier récupéré (« dernier entré, premier sorti » ou LIFO pour _last-in_, _first-out_ en anglais). Pour ajouter un élément sur la pile, utilisez la méthode `append()`{.py}. Pour récupérer l'objet au sommet de la pile, utilisez la méthode `pop()`{.py} sans indicateur de position. Par exemple :

```{.py}
>>> stack = [3, 4, 5]
>>> stack.append(6)
>>> stack.append(7)
>>> stack
[3, 4, 5, 6, 7]
>>> stack.pop()
7
>>> stack
[3, 4, 5, 6]
>>> stack.pop()
6
>>> stack.pop()
5
>>> stack
[3, 4]
```

Utilisation des listes comme des files
--------------------------------------

Il est également possible d'utiliser une liste comme une file, où le premier élément ajouté est le premier récupéré (« premier entré, premier sorti » ou FIFO pour _first-in_, _first-out_) ; toutefois, les listes ne sont pas très efficaces pour réaliser ce type de traitement. Alors que les ajouts et suppressions en fin de liste sont rapides, les opérations d'insertions ou de retraits en début de liste sont lentes (car tous les autres éléments doivent être décalés d'une position).

Pour implémenter une file, utilisez la classe `collections.deque`{.py} qui a été conçue pour réaliser rapidement les opérations d'ajouts et de retraits aux deux extrémités. Par exemple :

```{.py}
>>> from collections import deque
>>> queue = deque(["Eric", "John", "Michael"])
>>> queue.append("Terry")           # Terry arrives
>>> queue.append("Graham")          # Graham arrives
>>> queue.popleft()                 # The first to arrive now leaves
'Eric'
>>> queue.popleft()                 # The second to arrive now leaves
'John'
>>> queue                           # Remaining queue in order of arrival
deque(['Michael', 'Terry', 'Graham'])
```

Annexe 2 — exercice 1
=====================

(d'après « Relational Databases », Wikiversity
[`en.wikiversity.org/wiki/Relational_Databases`](https://en.wikiversity.org/wiki/Relational_Databases/Introduction#Tasks_to_try))

## Extrait des différentes relations de la base de données relationnelle _library_


Relation _Borrowers_
: \

ID Name Email              Phone
-- ---- ------------------ ---------
1  Sam  sam@internet       1234-5678
2  Alex alex@internet      2345-6789
3  Kim  kim@internet       3456-7890
4  Alex otheralex@internet 4567-8901

Relation _Books_
: \

ID Title                            Author                 Published
-- -------------------------------- ---------------------- ---------
1  Alice's Adventures in Wonderland Lewis Carroll          1865
2  The War of the Worlds            H. G. Wells            1898
3  The Time Machine                 H. G. Wells            1895
4  A Study in Scarlet               Sir Arthur Conan Doyle 1887

Relation _Loans_
: \

Book ID Borrower ID Date           Returned
------- ----------- -------------- -------------
1       1           1 January 1971 
2       2           1 March 1971   12 March 1971
3       2           1 March 1971 
4       3           1 May 1971 
2       3           14 March 1971 


<!-- eof --> 

Exercice 2 (6 points)
=====================

Ac ne quis a nobis hoc ita dici forte miretur

* Et eodem impetu Domitianum praecipitem per scalas
* Oportunum est, ut arbitror, explanare nunc causam.

<!--
 logos from
 https://www.python.org/community/logos/
 https://pixabay.com/vectors/snakes-animals-line-art-serpents-4524803/
-->

Des figures avec légende — et donc centrées et numérotées

![Logo thnx to
  [`python.org/community/logos/`](https://www.python.org/community/logos/)](img/python-logo.png){width=28%}

![Dessin thnx to
  [`pixabay.com/vectors/snakes-animals-line-art-serpents-4524803/`]( https://pixabay.com/vectors/snakes-animals-line-art-serpents-4524803/)](img/snakes.png){width=25%}

Une image 

![](img/python-logo.png){width=28%}

Centrée

-------------------------------
 ![](img/snakes.png){width=35%}
-------------------------------

Deux images côte à côte

------------------------------------    ------------------------------
 ![](img/python-logo.png){width=28%}     ![](img/snakes.png){width=35%}
------------------------------------    ------------------------------

Un saut de page manuel
<!-- MISE_EN_PAGE --> \newpage{}

En table

----------  ----------
Logo         ![](img/python-logo.png){width=28%}
Dessin       ![](img/snakes.png){width=25%}
----------  ----------

Une même image, de tailles différentes

---------------------------------- ---------------------------------- ---------------------------------- 
 ![](img/nsi-ipsum.png){width=10%}  ![](img/nsi-ipsum.png){width=25%}  ![](img/nsi-ipsum.png){width=40%}
---------------------------------- ---------------------------------- ---------------------------------- 


Partie A — Et eodem impetu Domitianum praecipitem per scalas
------------------------------------------------------------

question 1
: Unde Rufinus ea tempestate praefectus praetorio ad. Duplexque isdem
diebus acciderat malum, quod et. 

question 2
:   <!-- une espace suivie d'une espace insécable --> 

    1. Advenit post multos Scudilo Scutariorum tribunus
    2. Dein Syria per speciosam interpatet diffusa
    3. Et olim licet otiosae sint tribus pacataeque
    4. Ibi victu recreati et quiete, postquam abierat
    5. Et quia Montius inter dilancinantium manus

Ut enim benefici liberalesque sumus, non ut exigamus gratiam (neque enim beneficium faeneramur sed natura propensi ad liberalitatem sumus), sic amicitiam non spe mercedis adducti sed quod omnis eius fructus in ipso amore inest, expetendam putamus.
question 3
: Quid? qui se etiam nunc subsidiis patrimonii aut. Primi igitur
omnium statuuntur Epigonus et.

Partie B – Oportunum est, ut arbitror, explanare nunc causam
------------------------------------------------------------

Quam ob rem vita quidem talis fuit vel fortuna vel gloria, ut nihil
posset accedere, moriendi autem sensum celeritas abstulit.

Quo de genere mortis difficile dictu est; quid homines suspicentur,
videtis; hoc vere tamen licet dicere, P. Scipioni ex multis diebus,
quos in vita celeberrimos laetissimosque viderit, illum diem
clarissimum fuisse, cum senatu dimisso domum reductus ad vesperum est
a patribus conscriptis, populo Romano, sociis et Latinis, pridie quam
excessit e vita, ut ex tam alto dignitatis gradu ad superos videatur
deos potius quam ad inferos pervenisse. 

question 4
: Cum saepe multa, tum memini domi in hemicyclio. Has autem
provincias, quas Orontes ambiens amnis novo denique perniciosoque exemplo idem Gallus

question 5
: Quos metu voluptatum civitatis illuc domi agitant agmina latera agitant sine enim matronae varias inlecebras.




# Exercice 1

Cet exercice comporte trois parties :

1. Regnum amicitia illi Coriolano ob ; 
2. Propensior quaestor lenitatem haec sed si mollius ; 
3. Supra equitum ex quo apud emendabat.

## Partie 1 — Regnum amicitia illi Coriolano ob

Cette première partie revertuntur fulgente non sunt cum occultantes
nondum nauticos eum sentirent eisdem fame cum sunt.

Les détails quae in Appendice \pageref{anx1exo1} paginam. 

<!-- begin question 1 --> 
question 1
: Cette première question constantii acer Constantii in adlocutus
acer ut palatinarum et addensque consulens sonu custodiam palatinarum
praefecto post custodiam mollius super decere.
<!-- end question 1 -->

Denique Antiochensis ordinis vertices sub uno elogio iussit occidi
ideo efferatus, quod ei celebrari vilitatem intempestivam urgenti, cum
inpenderet inopia, gravius rationabili responderunt.

<!-- begin question 2 --> 
question 2
: Définir une fonction `decere()`{.py} qui accepte une valeur entière
en paramètre et renvoie cette millius super decere.
Ce duste avide est piget veneno facta populum est avide ordo.

    Cyprum itidem insulam procul a continenti discretam et portuosam
	inter municipia crebra urbes duae faciunt claram Salamis et
	Paphus.
<!-- end question 2 -->

Altera Iovis delubris altera Veneris templo insignis. tanta autem
tamque multiplici fertilitate abundat rerum omnium eadem.

```{.py}
def derive(ordres, regle):
    """
    Renvoie une liste obtenue en remplaçant chaque caractère 'F' de la
    chaîne ordres par tous les éléments de la chaine regle. 
    Contraintes : aucune
    Exemple :
    >>> derive('F+F', 'F-F')
    'F-F+F-F'
    """
    nouveaux_ordres = ''
    for pas in ordres:
        if pas == 'F':
            nouveaux_ordres = nouveaux_ordres + regle
        else:
            nouveaux_ordres = nouveaux_ordres + pas
    return nouveaux_ordres
```

<!-- begin question 3 -->
question 3
: Et licet quocumque oculos flexeris feminas adfatim coactique
aliquotiens nostri pedites ad eos. Itaque tum Scaevola cum in eam
ipsam mentionem.

    1. Donner ornatissimam maximis signis repulsum urbem maximis Asiam
       impetus erumpentem armatum exhausti custodita. 
	2. Écrire een conplurium Brittanniam possent Hispania longe quod
	   adfligens ferebatur sese et possent glabro Paulus quosdam sagax
	   membra quosdam sagax facinus. 
<!-- end question 3 -->

## Partie 2 – Propensior quaestor lenitatem haec sed si mollius.

Siquid siquid sui et modi hoc narrabimus siquid plagas insidias principem peiores nimia cautela aemulis nimia siquid ignotus medium dissimilem.

flagitiis  carnifex  aduliorum     adigius
---------- ------    ------------  -------
`abuis`    12        $\log n$      $\alpha$
`cluis`    143       $n^{n-1}$     $\varepsilon$
`munaem`   1         $\sqrt[n]{2}$ $\phi$

question 4
: Ne flagitiis carnifex adulteriorum ad. minabatur se discessurum: ut saltem id metuens perquisitor malivolus tandem desineret quieti coalitos homines in aperta pericula proiectare.

## Partie 3 – Supra equitum ex quo apud emendabat.

Batnae municipium in Anthemusia conditum Macedonum manu priscorum ab Euphrate flumine brevi spatio disparatur, refertum mercatoribus opulentis, ubi annua sollemnitate prope Septembris initium mensis ad nundinas magna promiscuae fortunae convenit multitudo ad commercanda quae Indi mittunt et Seres aliaque plurima vehi terra marique consueta.

question 5
: Flatus `[len(s) for s in ['sine', 'inanes', ['nascitur', 'quicd'], []]]`{.py}
opulentos postquam abierat equestrium recreati.

<!-- eof -->

Rédiger un exercice en Markdown
===============================

_Quelques précisons sur la syntaxe et les règles proposées et utilisées dans les exemples._

Au menu :

* saisir un exercice
* saisir une question
* saisir du code informatique
* référencer une page du sujet
* saisir des caractères "spéciaux"

→ Exercice
----------

Un exercice débute par un titre de niveau supérieur mentionnant le barème. On écrit

    # Exercice 3 (5 points)

ou

    Exercice 3 (5 points)
    =====================

Un exercice peut être découpé en parties identifiées par des titres de
second niveau. On numérote ces parties et on écrit par exemple :

    ## Première partie – Eius ianuis videri

ou

    Partie A — Eius ianuis videri
    -----------------------------

→ Question
----------

Chaque question est identifiée par un item comme ceci :

    question 4
    : Utque aegrum corpus quassari etiam levibus solet offensis, ita animus
	eius angustus et tener, quicquid increpuisset. 

Le texte de la question peut se poursuivre par d'éventuels autres paragraphes qui sont indentés de 4 espaces.

Le retour à l'indentation précédente marque la fin de la question :

    question 4
    : Utque aegrum corpus quassari etiam levibus solet offensis, ita animus
	eius angustus et tener, quicquid increpuisset. 

        Ad salutis suae dispendium existimans factum aut cogitatum,
        insontium caedibus fecit victoriam luctuosam.

    Denique Antiochensis ordinis vertices sub uno elogio iussit occidi
    ideo efferatus, quod ei celebrari vilitatem intempestivam urgenti,
    cum inpenderet inopia, gravius rationabili responderunt. 

Une question peut se subdiviser en plusieurs sous questions qui seront numérotées ainsi : 

    question 2
    : Et licet quocumque oculos flexeris feminas adfatim coactique
    aliquotiens nostri pedites ad eos. Itaque tum Scaevola cum in eam
    ipsam mentionem.
    
        1. Advenit post multos Scudilo Scutariorum tribunus
        2. Dein Syria per speciosam interpatet diffusa
        3. Et olim licet otiosae sint tribus pacataeque

→ Code informatique
-------------------

On écrit du code entre apostrophes inversées (backquotes). On peut préciser à la suite le langage du code. On écrira donc par exemple

    La variable `var`, la fonction `fonc()`, et l'expression 
	`[len(s) for s in ['sine', 'inanes', ['nascitur', 'quicd'], []]]`

ou, pour du Python 

    La variable `var`{.py}, la fonction `fonc()`{.py}, et l'expression 
	`[len(s) for s in ['sine', 'inanes', ['nascitur', 'quicd'], []]]`{.py}

On écrit un bloc de code entre triples apostrophes inversées :

    ```
    def derive(ordres, regle):
        """
        Renvoie une liste obtenue en remplaçant chaque caractère 'F' de la
        chaîne ordres par tous les éléments de la chaine regle. 
        """
        nouveaux_ordres = ''
        for pas in ordres:
            if pas == 'F':
                nouveaux_ordres = nouveaux_ordres + regle
            else:
                nouveaux_ordres = nouveaux_ordres + pas
        return nouveaux_ordres
    ```

On préfèrera préciser le langage, ici Python 

    ```{.py}
    def derive(ordres, regle):
        """
        Renvoie une liste obtenue en remplaçant chaque caractère 'F' de la
        chaîne ordres par tous les éléments de la chaine regle. 
        """
        nouveaux_ordres = ''
        for pas in ordres:
            if pas == 'F':
                nouveaux_ordres = nouveaux_ordres + regle
            else:
                nouveaux_ordres = nouveaux_ordres + pas
        return nouveaux_ordres
    ```

On peut numéroter les lignes de code :

    ```{.py .numberLines}
    def derive(ordres, regle):
        """
        Renvoie une liste obtenue en remplaçant chaque caractère 'F' de la
        chaîne ordres par tous les éléments de la chaine regle. 
        """
        nouveaux_ordres = ''
        for pas in ordres:
            if pas == 'F':
                nouveaux_ordres = nouveaux_ordres + regle
            else:
                nouveaux_ordres = nouveaux_ordres + pas
        return nouveaux_ordres
    ```


→ Label et référence
--------------------

On ajoute un label à un exercice ou une partie d'exercice en précisant
le nom de ce label entre `#{` e `}` à la suite du titre. Par exemple
sous l'une des formes suivantes :

    # Annexe 1 — exercice 3 {#anx1exo3}

ou

    Annexe 1 — exercice 3 {#anx1exo3}
    =================================

On utilise par ailleurs ce label pour produire le numéro de page avec
la commande `\pageref{}` :

    Singula quae in Appendice \pageref{anx1exo3} paginam. 

→ Caractères « spéciaux »
-------------------------

On utilise le codage UTF-8.  
Les caractères comme ç, œ, ou æ peuvent donc être saisi directement.

Les lettres greques peuvnet être saisies directement, ou en LaTeX
délimité par deux `$` :

    par exemple $\alpha$, $\varepsilon$ ou $\phi$

L'espace insécable est un caractère Unicode, elle est saisie
directement en UTF-8.

<!-- eof --> 

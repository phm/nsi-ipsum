Compiler un sujet
=================

1. Produire le PDF d'un sujet
1. Produire le PDF d'un exercice 
1. Quelques éléments de _making-of_

→ Produire le PDF d'un sujet
----------------------------

La production d'un fichier PDF à partir des sources Markdown est confiée à pandoc.

<!-- 
Il est donc nécessaire

* d'installer pandoc : [_Installing pandoc_ — pandoc.org/installing`](https://pandoc.org/installing.html)
* d'installer une distribution TeX ([TeX Live](http://www.tug.org/texlive/), sa variante []MaxTeX](http://www.tug.org/mactex/), ou autre).

-->

La configuration et les options choisies sont définies dans un fichier Makefile.

1. Récupérer le fichier LaTeX de configuration et de définition de la page de garde [`tmpl/nsi-header.tex`](../tmpl/nsi-header.tex)

1. Récupérer le modèle [`tmpl/Makefile`](../tmpl/Makefile)

1. Modifier les premières lignes qui indiquent
	* quels sont les fichiers Markdown à compiler 
    * quel est le fichier PDF à générer  
	 

    ```makefile
    # source des exercices et annexes - Markdown
    SOURCE = exo1 exo2 exo3 exo4 exo5 anx1 anx2 anx3 anx4 anx5
    # sujet à générer - PDF
    SUJET = sujet-nsi
	```

1. Invoquer la commande `make` suffit alors à produire le PDF :

    ```shell
	% make 
	```

→ Produire le PDF d'un exercice
-------------------------------

Dans une phase de mise au point, il est possible de produire un PDF pour un unique exercice écrit dans un fichier Markdown donné, par exemple 

```shell
% make exo2.pdf
```

compilera le fichier `exo2.md` pour produire `exo2.pdf`.

→ Quelques éléments de _making-of_
----------------------------------

_à venir_ 

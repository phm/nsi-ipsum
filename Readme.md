NSI Ipsum
=========

Modèle pour la rédaction en Markdown de sujets NSI dont la compilation via [pandoc](https://pandoc.org/) permet de produire un document PDF final. 

→ Exemple
---------

À partir des fichiers [`exo1.md`](../../raw/master/xampl/exo1.md), [`exo2.md`](../../raw/master/xampl/exo2.md), et [`anx1.md`](../../raw/master/xampl/anx1.md), il est possible de produire automatiquement le sujet [`xampl.pdf`](xampl/xampl.pdf).

L'invocation de pandoc avec les options ad hoc et l'utilisation du
fichier d'entête se font via le [`Makefile`](xampl/Makefile). 

→ Rédaction en Markdown
-----------------------

Un sujet est une séquence d'exercices et d'annexes.

Il est proposé de fournir un fichier Markdown par exercice et par annexe.

Un fichier Markdown est un fichier texte qu'il convient de produire à l'aide d'un éditeur de texte. Aucun environnement, visualisateur de Markdown ou éditeur spécifique n'est nécessaire, ni recommandé. 

Concernant la syntaxe Markdown, on s'inspirera des exemples du répertoire [`xampl/`](.:xampl/) qui suivent les règles énoncées sur la page

* [_Rédiger un exercice en Markdown_ — `doc/syntax.md`](doc/syntax.md)

On consultera éventuellement la documentation de pandoc à [_Pandoc User’s Guide_ — `pandoc.org/MANUAL`](https://pandoc.org/MANUAL.html#pandocs-markdown). 

→ Production d'un fichier PDF
-----------------------------

La production d'un fichier PDF à partir des fichiers Markdown est réalisée via la commande [pandoc](https://pandoc.org/).  
Une manière de procéder est décrite sur la page 

* [_Compiler un sujet_ — `doc/compile.md`](doc/compile.md)

→ Contenu
---------

* [`Readme.md`](./Readme.md) — le présent fichier ;) 
* [`doc`](./doc) — documentation 
* [`tmpl`](./tmpl) — template
* [`xampl`](./xampl) — exemple

